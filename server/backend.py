###############################################################################
##
##  Copyright (C) 2014 Tavendo GmbH
##
##  Licensed under the Apache License, Version 2.0 (the "License");
##  you may not use this file except in compliance with the License.
##  You may obtain a copy of the License at
##
##      http://www.apache.org/licenses/LICENSE-2.0
##
##  Unless required by applicable law or agreed to in writing, software
##  distributed under the License is distributed on an "AS IS" BASIS,
##  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
##  See the License for the specific language governing permissions and
##  limitations under the License.
##
###############################################################################

from twisted.internet.defer import inlineCallbacks

from autobahn.twisted.util import sleep
from autobahn.twisted.wamp import ApplicationSession

import time, datetime

from math import sqrt

from candles import CandleStore
from ornsteinuhlenbeck import OrnsteinUhlenbeck
from externalweightedprices import ExternalWeightedPrices

TIMESTEP = 1 # refresh rate
TIMESTEP_YEAR = TIMESTEP * 1.0 / 365/24/60/60

candleFrequency = 60 # frequency of candles
candleTickers = ['BTCX','LTCX','PPCX']
candleFname = 'candlestore.json'
candleStore = CandleStore.fromFile(candleFname)
if candleStore is None:
  candleStore = CandleStore(candleFrequency, candleTickers)

ouSigmaAsymptotic = 0.1 # we want +/- 0.1 on top of price. Var(x) = sigma^2/2/theta
ouTheta = 365.0*24*60 # 1 min mean reversion
ouSigma = 2*sqrt(ouTheta)*ouSigmaAsymptotic
ou = OrnsteinUhlenbeck(ouSigma, ouTheta)

externalWeightedPrices = ExternalWeightedPrices()

class Component(ApplicationSession):
   @inlineCallbacks
   def onJoin(self, details):

      # registering rpc function
      def getPriceHistory(ticker):
         return candleStore.candleStore[ticker].toJson()

      yield self.register(getPriceHistory, 'com.ecurex.history')

      # pushing on pubsub
      while True:
         t0 = time.clock()
         timestamp = int(time.mktime(datetime.datetime.now().timetuple()))
         indices = externalWeightedPrices.get()
         noise = ou.getNext(TIMESTEP_YEAR)
         for ticker in indices.keys():
           index = indices[ticker] + noise
           if(index == 0): continue
           candleStore.addPrice(ticker, index, timestamp)
           self.publish('com.ecurex.'+ticker.lower(), candleStore.getLast(ticker).toJson())
         candleStore.toFile(candleFname)
         dt = time.clock() - t0
         sleep_time = TIMESTEP - dt
         if(sleep_time<0): sleep_time = 0
         yield sleep(sleep_time)

if __name__ == '__main__':
   from autobahn.twisted.wamp import ApplicationRunner
   runner = ApplicationRunner("ws://127.0.0.1:8081/ws", "realm1")
   runner.run(Component)
