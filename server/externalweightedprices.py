from market_query import BTCeAPI 
from math import pow
from datetime import datetime

class ExternalWeightedPrices(object):
  # gets the weighted price from an external source
  #BTCX = ( (BTC/LTC)^w_LTC * (BTC/PPC)^w_PPC ) ^ (1 / (w_LTC+w_PPC)) 
  #LTCX = ( (LTC/BTC)^w_BTC * (LTC/PPC)^w_PPC ) ^ (1 / (w_BTC+w_PPC)) 
  #PPCX = ( (PPC/BTC)^w_BTC * (PPC/LTC)^w_LTC ) ^ (1 / (w_BTC+w_LTC)) 
  def __init__(self):
    self.ltcbtc = BTCeAPI("LTC","BTC")
    self.ppcbtc = BTCeAPI("PPC","BTC")
    self.ppcltc = BTCeAPI("PPC","LTC")
    self.w_BTC = 1.0
    self.w_LTC = 1.0
    self.w_PPC = 1.0
    self.BTCX = 0
    self.LTCX = 0
    self.PPCX = 0
  def _updateIndices(self):
    LTCBTC = self.ltcbtc.get_ticker()['last']
    BTCLTC = 1 / LTCBTC
    PPCBTC = self.ppcbtc.get_ticker()['last']
    BTCPPC = 1 / PPCBTC
    PPCLTC = PPCBTC / LTCBTC
    LTCPPC = 1 / PPCLTC
    self.BTCX = pow(pow(BTCLTC, self.w_LTC) * pow(BTCPPC, self.w_PPC), 1.0/(self.w_LTC+self.w_PPC))
    self.LTCX = pow(pow(LTCBTC, self.w_BTC) * pow(LTCPPC, self.w_PPC), 1.0/(self.w_BTC+self.w_PPC))
    self.PPCX = pow(pow(PPCBTC, self.w_BTC) * pow(PPCLTC, self.w_LTC), 1.0/(self.w_BTC+self.w_LTC))
  def get(self):
    try:
      self._updateIndices()
    except Exception,e:
      print datetime.now(), str(e)
      pass
    return {'BTCX':self.BTCX,'LTCX':self.LTCX,'PPCX':self.PPCX}

