import json

class Candle(object):
  def __init__(self, price, tsStart):
    self.open = price
    self.close = price
    self.low = price
    self.high = price
    self.tsStart = tsStart # timestamp of start of candle 

  def add(self, price):
    if(self.low > price): self.low = price
    if(self.high < price): self.high = price
    self.close = price

  def toJson(self):
    return {'x':self.tsStart*1000, 'open':self.open, 'high':self.high, 'low':self.low, 'close':self.close, 'id':self.tsStart}

  @staticmethod
  def fromJson(j):
    candle = Candle(None,None)
    candle.open = j['open']
    candle.close = j['close']
    candle.high = j['high']
    candle.low = j['low']
    candle.tsStart = j['x']/1000
    return candle

  def __str__(self):
    return str(self.toJson())

  def __repr__(self):
    return self.__str__()

CANDLELIST_MAXSIZE = 1000
class CandleList(object):
  def __init__(self, frequency):
    self.frequency = frequency
    self.candles = []

  def _tsStart(self, timestamp):
    # finds timestamp of beginning of period
    if self.frequency > 24*60*60:
      raise Exception('implement this method for frequencies higher than a day')
    return timestamp - timestamp % self.frequency

  def addPrice(self, price, timestamp):
    tsStart = self._tsStart(timestamp)
    lastCandle = self.getLast()
    if lastCandle == None:
      self.candles.append(Candle(price, tsStart))
      return
    lastCandle.add(price)
    if(lastCandle.tsStart < tsStart): # candle out of date
      self.candles.append(Candle(price, tsStart)) # add new candle
      while(len(self.candles)>CANDLELIST_MAXSIZE): self.candles.pop(0) # remove first elements if size exceeded

  def toJson(self):
    j = {'frequency':self.frequency,'candles':[]}
    for candle in self.candles:
      j['candles'].append(candle.toJson())
    return j

  @staticmethod
  def fromJson(j):
    candleList = CandleList(None)
    candleList.frequency = j['frequency']
    candleList.candles = []
    for r in j['candles']:
      c = Candle.fromJson(r)
      candleList.candles.append(c)
    return candleList

  def getLast(self):
    if(len(self.candles)>0): return self.candles[-1]
    else: return None

class CandleStore(object):
  def __init__(self, frequency, tickers):
    """
    frequency: lenght of one candle in seconds
    tickers: list of ticker strings that are stored
    """
    self.candleStore = {}
    for ticker in tickers:
      self.candleStore[ticker] = CandleList(frequency)

  def addPrice(self, ticker, price, timestamp):
    self.candleStore[ticker].addPrice(price, timestamp)

  def toJson(self):
    j = {}
    for ticker in self.candleStore.keys():
      j[ticker] = self.candleStore[ticker].toJson()
    return j

  @staticmethod
  def fromJson(j):
    print j
    candleStore = CandleStore(None,[None])
    candleStore.candleStore = {}
    for ticker in j.keys():
      candleStore.candleStore[ticker] = CandleList.fromJson(j[ticker])
    return candleStore

  def toFile(self, fname):
    with open(fname, 'w') as outfile:
      json.dump(self.toJson(), outfile)

  @staticmethod
  def fromFile(fname):
    try: j = json.load(open(fname))
    except: return None
    return CandleStore.fromJson(j)

  def getLast(self, ticker):
    return self.candleStore[ticker].getLast()

if __name__ == '__main__':
  c = CandleStore(10, ['BTCX','LTCX'])
  c.addPrice('BTCX', 3.14, 0)
  c.addPrice('BTCX', 3.14, 10)
  c.addPrice('BTCX', 3.5, 11)
  c.addPrice('BTCX', 3.1, 12)
  c.addPrice('BTCX', 3.3, 13)
  c.addPrice('BTCX', 3.4, 20)
  c.addPrice('BTCX', 3.4, 31)
  c.addPrice('LTCX', 33.14, 0)
  c.addPrice('LTCX', 33.14, 10)
  c.addPrice('LTCX', 33.5, 11)
  c.addPrice('LTCX', 33.1, 12)
  c.addPrice('LTCX', 33.3, 13)
  c.addPrice('LTCX', 33.4, 20)
  c.addPrice('LTCX', 33.4, 31)
  j = c.toJson()
  assert(j == CandleStore.fromJson(j).toJson())
