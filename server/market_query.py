#!/usr/bin/python


#Importing modules
import json
import urllib2 

from urllib import urlencode
import time
from hashlib import sha512
from hmac import HMAC
import base64
import sqlite3 as sql
import sys,os
 

class BTCeAPI:
  def __init__(self, curr1, curr2):
    self.curr1 = curr1.lower()
    self.curr2 = curr2.lower()

  def get_fee(self):
    url = "https://btc-e.com/api/2/%s_%s/fee"%(self.curr1, self.curr2)
    data = json.load(urllib2.urlopen(url))
    return data["trade"]
 
  def get_ticker(self):
    url = "https://btc-e.com/api/2/%s_%s/ticker"%(self.curr1, self.curr2)
    data = json.load(urllib2.urlopen(url))
    #  u'sell': 107.409, u'buy': 107.87, u'last': 107.87, u'vol': 813874.23411, 
    #  u'vol_cur': 7761.93462, u'high': 108.34, u'low': 99.9, 
    #  u'server_time': 1368727634, u'avg': 104.12}

    if data.has_key('error'):
      raise Exception('Error in market_query: '+data['error'])
    return data["ticker"] 

  def get_conversion(self):
    d = self.get_ticker()
#    print d.keys()
    self.ask, self.bid = d["buy"], d["sell"]
    return self.ask, self.bid # ask, bid



  def get_depth(self):
    url = "https://btc-e.com/api/2/%s_%s/depth"%(self.curr1, self.curr2)
    data = json.load(urllib2.urlopen(url))
    asks = sorted( data["asks"] )
    
    bids = sorted( data["bids"])
    bids.reverse()
    
    self.asks_depth, self.bids_depth =  ( asks, bids )
    return ( asks, bids )




########################################################################################

class CryptsyAPI:

    dict_markets = {"LTCBTC":3,"XPMBTC":63,"XPMLTC":106, "PPCBTC":28, "NMCBTC":29, "PPCLTC":125}

    def __init__(self, curr1, curr2):
        self.curr1 = curr1.upper()
        self.curr2 = curr2.upper()
        self.market = self.dict_markets[ self.curr1+self.curr2 ]
 

    def get_conversion(self):
        url = "http://pubapi.cryptsy.com/api.php?method=singlemarketdata&marketid=%s"%(self.market)
#        print url
        data = json.load(urllib2.urlopen(url))
        data = data['return']['markets'][self.curr1]
        self.ask = max( [float(o['price']) for o in data['sellorders']])
        self.bid = min( [float(o['price']) for o in data['buyorders']])

        #ask = data['value']
        #url = "https://vircurex.com/api/get_highest_bid.json?base=%s&alt=%s"%(self.curr1, self.curr2)
        #data = json.load(urllib2.urlopen(url))
        #bid = data['value']
    
        #self.ask, self.bid = float(ask),  float(bid)# ask, bid
        return self.ask, self.bid 

    def get_depth(self):
        url = "http://pubapi.cryptsy.com/api.php?method=singlemarketdata&marketid=%s"%(self.market)
#        print url
        data = json.load(urllib2.urlopen(url))
        data = data['return']['markets'][self.curr1]
        asks =  sorted( [(float(o['price']), float(o['quantity'])) for o in data['sellorders']] )
        bids =  sorted( [(float(o['price']), float(o['quantity'])) for o in data['buyorders']] )
        
        bids.reverse()
   
        self.asks_depth, self.bids_depth =  ( asks, bids )
        return ( asks, bids )





########################################################################################
########################################################################################





class VircurexAPI:

    def __init__(self, curr1, curr2):
        self.curr1 = curr1.upper()
        self.curr2 = curr2.upper()
 

    def get_conversion(self):
        url = "https://vircurex.com/api/get_lowest_ask.json?base=%s&alt=%s"%(self.curr1, self.curr2)
        data = json.load(urllib2.urlopen(url))
        ask = data['value']
        url = "https://vircurex.com/api/get_highest_bid.json?base=%s&alt=%s"%(self.curr1, self.curr2)
        data = json.load(urllib2.urlopen(url))
        bid = data['value']
    
        self.ask, self.bid = float(ask),  float(bid)# ask, bid
        return self.ask, self.bid 

    def get_depth(self):
        url = "https://vircurex.com/api/orderbook.json?base=%s&alt=%s"%(self.curr1, self.curr2)
        data = json.load(urllib2.urlopen(url))
        
        asks = sorted( [ [float(p),float(v)] for [p,v] in data["asks"] ] )
        
        bids = sorted( [ [float(p),float(v)] for [p,v] in data["bids"] ] )
        bids.reverse()
   
        self.asks_depth, self.bids_depth =  ( asks, bids )
        return ( asks, bids )




########################################################################################
########################################################################################
########################################################################################

########################################################################################
########################################################################################
########################################################################################





class BterAPI:

    def __init__(self, curr1, curr2):
        self.curr1 = curr1.lower()
        self.curr2 = curr2.lower()
 

    def get_conversion(self):
        url = "https://bter.com/api/1/ticker/%s_%s"%(self.curr1, self.curr2)
        data = json.load(urllib2.urlopen(url))
        assert data['result'] == "true"

        self.ask, self.bid = data["sell"], data["buy"] # ask, bid
        return self.ask, self.bid 
    
    def get_depth(self):
        url = "https://bter.com/api/1/depth/%s_%s"%(self.curr1, self.curr2)
        
        data = json.load(urllib2.urlopen(url))
        assert data['result'] == "true"
        
        asks = sorted( [ [float(p),float(v)] for [p,v] in data["asks"] ] )
        
        bids = sorted( [ [float(p),float(v)] for [p,v] in data["bids"] ] )
        bids.reverse()
        
        self.asks_depth, self.bids_depth =  ( asks, bids )
        return ( asks , bids )


 
