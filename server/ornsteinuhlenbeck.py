from random import randint
from math import sqrt

class OrnsteinUhlenbeck(object):
  def __init__(self, sigma, theta):
    self.sigma = sigma
    self.theta = theta
    self.x = 0

  def getNext(self, dt):
    dW = randint(0,1)+randint(0,1)+randint(0,1)+randint(0,1) - 2
    dx = -self.theta*self.x*dt + self.sigma * sqrt(dt) * dW
    self.x += dx
    return self.x

