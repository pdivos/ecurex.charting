$(function () {
    var connection = new autobahn.Connection({
       url: 'ws://95.138.181.101:8081/ws',
       realm: 'realm1'}
    );
    var chart = null;
    var ids;
        
    connection.onopen = function (session) {
    
       function onLivePriceArrived(args) {
           console.log(args[0]['close'])
           if(chart == null) return;
           var candle = args[0];
           var newestPoint = chart.get(ids[ids.length - 1]);
           if(newestPoint['id'] != candle['id']) { // if the latest candle is a new one, then...
               // remove old candles such that we have no more than 1000
               while(ids.length >= 1000) {
                   chart.get(ids[0]).remove();
                   ids.splice(0, 1);
               }
               // add new candle
               ids.push(candle['id']);
               chart.series[0].addPoint(candle, false);
           } else { // update newest point
               newestPoint.update(candle);
           }
           chart.series[0].update(); // update chart
       }
    
       function onPriceHistoryArrived(priceHistory) {
           priceHistory = priceHistory['candles'];
           chart = createChart(priceHistory);
           ids = [];
           priceHistory.forEach(function(entry) {
               ids.push(entry['id']);
           });
       }
    
       function getPriceHistory() {
          // tickers: 'BTCX', 'LTCX', 'PPCX'
          session.call('com.ecurex.history', ['BTCX']).then(
             function (res) {
                onPriceHistoryArrived(res);
             }
          )
       }
       getPriceHistory();
       // channels: com.ecurex.btcx, com.ecurex.ltxc, com.ecurex.ppcx
       session.subscribe('com.ecurex.btcx', onLivePriceArrived);
    };
    
    connection.open()
    
    function createChart(data) {
            // create the chart
            $('#container').highcharts('StockChart', {
    
                title: {
                    text: 'Live BTCX from BTCe'
                },
    
                rangeSelector : {
                    buttons : [{
                        type : 'hour',
                        count : 1,
                        text : '1h'
                    }, {
                        type : 'day',
                        count : 1,
                        text : '1D'
                    }, {
                        type : 'all',
                        count : 1,
                        text : 'All'
                    }],
                    selected : 1,
                    inputEnabled : false
                },
    
                series : [{
                    name : 'BTCUSD',
                    type: 'candlestick',
                    data : data,
                    tooltip: {
                        valueDecimals: 2
                    }
                }]
            });
        return $('#container').highcharts();
    }
});

