Live BTCX, LTCX and PPCX index computation. Consists of three parts:

1. a back-end which is in server/backend.py. This queries BTCe once a second, computes the indices and pushes the results to the wamp pubsub, also exposes an rpc call for the histories. It stores the histories in a file called candlestore.json which is read on startup and updated at every second. It includes 4 python modules:
 - market_query.py, this is Claudio's function to grab the latest price from BTCe
 - candles.py, this contanis three classes: Candle which is a single candle, CaneldList which is a historical list of candles and CandleStore which sontains the candle hostories for all 3 indices. These classes have functgionalities to write and read themselves to/form json.
 - externalweightedprices.py, this one contains a class that does the geometric weighting.
 - ornsteinuhlenbeck.py, this one runs an OU process that is used as noise.

2. a wamp router wamp_router.py which is a standard autobanh example file, I didnt modify it.

3. a http_server.py which serves static content which is in the client folder. Within the client folder there is a frontend.html that embeds frontend.js that is the client side for the autobanh pubsub and rpc that grabs and updates the chart regularly.

To start all these at once, run start_server.sh. Stop with stop_server.sh and restart with restar_server.sh.